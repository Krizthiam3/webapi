﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppoitmentApp.Models
{
    public class Patient
    {
        public string id { get; set; }
        public string history { get; set; }
        public string identification { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string genre { get; set; }
        public string civil_status { get; set; }
        public string blood_type { get; set; }
        public string date_birth { get; set; }
        public string url { get; set; }    

    }
}