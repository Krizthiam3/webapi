﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace AppoitmentApp.Models
{
    public class DoctorRepository : IDoctorRepository
    {
        WebClient wc = new WebClient();

        public IEnumerable<Doctor> GetAll()
        {
            try
            {
                var json = wc.DownloadString("http://pruebas.apimedic.personalsoft.net:8082/api/v1/doctors/");
                return JsonConvert.DeserializeObject<List<Doctor>>(json);
            }
            catch (Exception)
            {
                return new List<Doctor>();
            }
         
        }

        public Doctor Get(string id)
        {
            try
            {
                var json = wc.DownloadString("http://pruebas.apimedic.personalsoft.net:8082/api/v1/doctors/" + id);
                return JsonConvert.DeserializeObject<Doctor>(json);

            }
            catch (Exception)
            {

                return new Doctor();
            }
          
        }
    }
}