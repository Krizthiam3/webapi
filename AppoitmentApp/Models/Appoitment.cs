﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppoitmentApp.Models
{
    public class Appoitment
    {
        public string Id { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
        public string IdPaciente { get; set; }
        public string IdMedico { get; set; }
        public string IdEstado { get; set; }
    }
}