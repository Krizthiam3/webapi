﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppoitmentApp.Models
{
    public interface IAppoitmentRepository
    {
        IEnumerable<Appoitment> GetAll();
        Appoitment Get(string id);
        IEnumerable<Appoitment> GetCitasMedico(string idMedico);
        IEnumerable<Appoitment> Post(Appoitment item);
        IEnumerable<Appoitment> Put(string id, Appoitment item);
        IEnumerable<Appoitment> Delete(string id);
    }
}