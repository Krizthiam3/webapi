﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppoitmentApp.Models
{
    public class Doctor
    {
        public string id { get; set; }
        public string identification { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string url { get; set; }
        public string blood_type { get; set; }
        public specialty specialty_field { get; set; }       
    }

    public class specialty
    {
        public string id { get; set; }
        public string url { get; set; }
        public string specialty_type { get; set; }
    }
}