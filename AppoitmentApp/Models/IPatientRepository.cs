﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppoitmentApp.Models
{
    public interface IPatientRepository
    {
        IEnumerable<Patient> GetAll();
        Patient Get(string id);
    }
}