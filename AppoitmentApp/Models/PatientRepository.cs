﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace AppoitmentApp.Models
{
    public class PatientRepository : IPatientRepository
    {
        WebClient wc = new WebClient();

        public IEnumerable<Patient> GetAll()
        {         
            try
            {
                var json = wc.DownloadString("http://pruebas.apimedic.personalsoft.net:8082/api/v1/patients/");
                return JsonConvert.DeserializeObject<List<Patient>>(json);
            }
            catch (Exception)
            {
                return new List<Patient>();              
            }
        }

        public Patient Get(string id)
        {
            try
            {
                var json = wc.DownloadString("http://pruebas.apimedic.personalsoft.net:8082/api/v1/patients/" + id);
                return JsonConvert.DeserializeObject<Patient>(json);
            }
            catch (Exception)
            {
                return new Patient();              
            }        
        }
    }
}