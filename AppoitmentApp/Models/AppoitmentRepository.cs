﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppoitmentApp.Models
{
    public class AppoitmentRepository : IAppoitmentRepository
    {
        List<Appoitment> _lst = new List<Appoitment>();

        public IEnumerable<Appoitment> GetAll()
        {
            _lst = new List<Appoitment>();

            _lst.Add(new Appoitment() { Id = "1", Fecha = "2015-10-01", Hora = "10:00", IdPaciente = "12", IdMedico = "1", IdEstado = "01" });
            _lst.Add(new Appoitment() { Id = "2", Fecha = "2015-10-01", Hora = "10:00", IdPaciente = "13", IdMedico = "3", IdEstado = "01" });
            _lst.Add(new Appoitment() { Id = "3", Fecha = "2015-10-01", Hora = "10:00", IdPaciente = "14", IdMedico = "4", IdEstado = "01" });
            _lst.Add(new Appoitment() { Id = "4", Fecha = "2015-10-01", Hora = "10:00", IdPaciente = "15", IdMedico = "5", IdEstado = "01" });
            _lst.Add(new Appoitment() { Id = "5", Fecha = "2015-10-01", Hora = "10:00", IdPaciente = "16", IdMedico = "6", IdEstado = "01" });

            return _lst;
        }

        
        public IEnumerable<Appoitment> GetCitasMedico(string idMedico)
        {                       
            return _lst.Where(c => c.IdMedico.Equals(idMedico));
        }

        public Appoitment Get(string id)
        {
            Appoitment item = new Appoitment();
            item = _lst.Find(c => c.Id.Equals(id));
            return item;
        }

        public IEnumerable<Appoitment> Post(Appoitment item)
        {
            _lst.Add(item);
            return _lst;
        }

        public IEnumerable<Appoitment> Put(string id, Appoitment item)
        {
            var appoitment = _lst.Where(x => x.Id == id).FirstOrDefault();

            if (appoitment != null)
            {
                appoitment.Fecha = item.Fecha;
                appoitment.Hora = item.Hora;
                appoitment.IdMedico = item.IdMedico;
                appoitment.IdPaciente = item.IdPaciente;
                appoitment.IdEstado = item.IdEstado;
            }
         
            return _lst;
        }

        public IEnumerable<Appoitment> Delete(string id)
        {   
            _lst.Remove(_lst.Find(c => c.Id.Equals(id)));
            return _lst;         
        }
    }
}