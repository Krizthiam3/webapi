﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppoitmentApp.Models
{
    public interface IDoctorRepository
    {
        IEnumerable<Doctor> GetAll();
        Doctor Get(string id);
    }
}