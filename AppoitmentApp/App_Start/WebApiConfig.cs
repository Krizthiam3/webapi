﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Configuration;

namespace AppoitmentApp
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de API web

            // Rutas de API web
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApiV1",
                routeTemplate: "api/v1/{controller}/{id}",
               defaults: new { id = RouteParameter.Optional }
            );         


            //config.Routes.MapHttpRoute(
            //    name: "DefaultApiV2",
            //    routeTemplate: "api/v2/{controller}/{id}",
            //    defaults: new {  id = RouteParameter.Optional }
            //);

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
           
        }
    }
}
