﻿using AppoitmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppoitmentApp.Controllers
{
    public class DoctorController : ApiController
    {
        private static DoctorRepository Repository = new DoctorRepository();

        /// <summary>
        ///  Retorna el listado de Doctores.
        /// </summary>
        public IEnumerable<Doctor> GetAllDoctor()
        {
            return Repository.GetAll();
        }

        /// <summary>
        ///  Retorna un doctor por id
        /// </summary>
        /// <param name="id">El ID del medico.</param>
        public Doctor GetDoctor(string id)
        {
            return Repository.Get(id);
        }
    }
}
