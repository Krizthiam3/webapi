﻿using AppoitmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppoitmentApp.Controllers
{
    public class PatientController : ApiController
    {
        private static PatientRepository RepositoryPaciente = new PatientRepository();

        /// <summary>
        ///  Retorna el listado de pacientes.
        /// </summary>
        public IEnumerable<Patient> GetAllPatients()
        {
            return RepositoryPaciente.GetAll();
        }

        /// <summary>
        ///  Retorna un paciente por id.
        /// </summary>
        /// <param name="id">El ID del paciente.</param>
        public Patient GetPatient(string id)
        {
            return RepositoryPaciente.Get(id);
        }
    }
}
