﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppoitmentApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Doctor()
        {
            ViewBag.Title = "Doctor Page";

            return View();
        }

        public ActionResult Patient()
        {
            ViewBag.Title = "Patient Page";

            return View();
        }
    }
}
