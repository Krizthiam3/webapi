﻿using AppoitmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace AppoitmentApp.Controllers
{
    [RoutePrefix("Appoitments")]
    public class AppoitmentController : ApiController
    {
        private static AppoitmentRepository Repository = new AppoitmentRepository();

        /// <summary>
        ///  Retorna el listado de citas.
        /// </summary>
        public IEnumerable<Appoitment> GetAllAppoitments()
        {
            return Repository.GetAll();
        }

        /// <summary>
        /// Retorna una cita por Id Cita
        /// </summary>
        /// <param name="id">El ID de la cita.</param>

        [ActionName("GetById")]
        public Appoitment GetAppoitment(string id)
        {
            return Repository.Get(id);
        }

        /// <summary>
        ///  Retorna el listado de citas de un medico. 
        /// </summary>
        /// /// <param name="id">El ID del medico.</param>

        [ActionName("GetByDoctor")]
        public IEnumerable<Appoitment> GetAppoitmentsDoctor(string idMedico)
        {
            return Repository.GetCitasMedico(idMedico);          
        }

        /// <summary>
        ///  Crea una nueva cita
        /// </summary>
        public IEnumerable<Appoitment> PostAppoitment(Appoitment item)
        {
            return Repository.Post(item);             
        }

        /// <summary>
        ///  Modifica una cita
        /// </summary>
        /// <param name="id">El ID de la cita.</param>
        /// <param name="item">Entidad cita.</param>
        public IEnumerable<Appoitment> PutAppoitment(string id, Appoitment item)
        {           
           return Repository.Put(id, item);          
        }

        /// <summary>
        ///  Eliminar una cita
        /// </summary>
        /// <param name="id">El ID de la cita.</param>
        public IEnumerable<Appoitment> DeleteAppoitment(string id)
        {
           return Repository.Delete(id);
        }
    }
}

